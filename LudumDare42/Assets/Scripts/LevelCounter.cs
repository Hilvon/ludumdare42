﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelCounter : MonoBehaviour {
    public static void IncreaseLevel() {
        currentLevel++;
        if (onIncreased != null) onIncreased();
    }
    public static void ResetLevel() {
        currentLevel = 0;
    }
    static event System.Action onIncreased;
    public int RequiredLevel;
    public UnityEvent OnLevelReached;
    public static int Current { get { return currentLevel; }}
    static int currentLevel = 0;
    private void Start()
    {
        onIncreased += CheckLevel;
    }
    void CheckLevel() {
        if (currentLevel >= RequiredLevel) {
            OnLevelReached.Invoke();
        }
    }

}
