﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenablePanel : MonoBehaviour {

    public void Show() {
        gameObject.SetActive(true);
    }
    public void Hide() {
        gameObject.SetActive(false);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
