﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HostTycoon
{
    public class ServerModels : MonoBehaviour
    {
        const int MaxModels = 99;

        static int NextID;
        public static ServerModels GetModel(int Id)
        {
            return new ServerModels() { ID = Id };
        }
        static int GetVacantID()
        {
            return NextID++;
        }
        public static void Reset()
        {
            NextID = 0;
        }
        public static ServerModels Create()
        {
            return GetModel(GetVacantID());
        }

        int ID;

        float[] speed = new float[MaxModels];
        float[] storage = new float[MaxModels];
        float[] reliability = new float[MaxModels];

    }
}