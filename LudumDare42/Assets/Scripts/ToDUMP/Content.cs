﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HostTycoon
{

    public class Content
    {
        const int MaxContent = 100000000;
        static int GetVacantID()
        {
            for (int i = 0; i < MaxContent; i++)
            {
                if (author[i] == -1) return i;
            }
            return 0;
        }
        public static void Reset()
        {
            for (int i = 0; i < MaxContent; i++)
            {
                author[i] = -1;
            }
        }
        public static Content Create()
        {
            return GetContent(GetVacantID());
        }
        public static Content GetContent(int ID)
        {
            if (wrappers[ID] == null) wrappers[ID] = new Content() { ID = ID };
            return wrappers[ID];
        }
        static int[] author = new int[MaxContent];
        static int[] cluster = new int[MaxContent];
        static float[] length = new float[MaxContent];
        static float[] density = new float[MaxContent];
        static float[] quality = new float[MaxContent];
        static int[] topics = new int[MaxContent];
        static int[] likes = new int[MaxContent];
        static int[] dislikes = new int[MaxContent];
        static int[] views = new int[MaxContent];
        static Content[] wrappers = new Content[MaxContent];

        int ID;

        public int Author {
            get { return author[ID]; }
            set { author[ID] = value; }
        }
        public int Cluster {
            get { return cluster[ID]; }
            set { cluster[ID] = value; }
        }
        public int Topics {
            get { return topics[ID]; }
            set { topics[ID] = value; }
        }
        public int Likes {
            get { return likes[ID]; }
            set { likes[ID] = value; }
        }
        public int Dislikes {
            get { return dislikes[ID]; }
            set { dislikes[ID] = value; }
        }
        public int Views {
            get { return views[ID]; }
            set { views[ID] = value; }
        }
        public float Length {
            get { return length[ID]; }
            set { length[ID] = value; }
        }
        public float Density {
            get { return density[ID]; }
            set { density[ID] = value; }
        }
        public float Quality {
            get { return quality[ID]; }
            set { quality[ID] = value; }
        }
        //public static Content[] Contents = new Content[10000000];
        //static Stack<int> Deleted = new Stack<int>();
        //public static int AssignIndex()
        //{
        //    if (Deleted.Count > 0) return Deleted.Pop();
        //    return MaxActive++;
        //}
        //public static int MaxActive { get; private set; }
        //public static void Delete(int I)
        //{
        //    Deleted.Push(I);
        //    Contents[I].IsActive = false;
        //}
        //public static void Activate(int I)
        //{
        //    Contents[I].IsActive = true;
        //}
        //bool IsActive;


    }
}