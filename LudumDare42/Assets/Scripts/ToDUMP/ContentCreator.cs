﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HostTycoon
{
    public class ContentCreator
    {
        const int MaxCreators = 1000;

        static int[] temper = new int[MaxCreators];
        static int[] topic = new int[MaxCreators];
        static float[] skill = new float[MaxCreators];
        static float[] format = new float[MaxCreators];
        static float[] productivity = new float[MaxCreators];
        static float[] motivation = new float[MaxCreators];
        static float[] popularity = new float[MaxCreators];
        static float[] nextCreationSize = new float[MaxCreators];
        static float[] nextCreationProgress = new float[MaxCreators];

        static ContentCreator[] wrappers = new ContentCreator[MaxCreators];

        static int GetVacantID()
        {
            for (int i = 0; i < MaxCreators; i++)
            {
                if (temper[i] == -1) return i;
            }
            return 0;
        }
        public static void Reset()
        {
            for (int i = 0; i < MaxCreators; i++)
            {
                temper[i] = -1;
            }
        }
        public static ContentCreator Create()
        {
            return GetContent(GetVacantID());
        }
        public static ContentCreator GetContent(int ID)
        {
            if (wrappers[ID] == null) wrappers[ID] = new ContentCreator() { ID = ID };
            return wrappers[ID];
        }




        int ID;
        public int Topic {
            get { return topic[ID]; }
            set { topic[ID] = value; }
        }
        public int Temper {
            get { return temper[ID]; }
            set { temper[ID] = value; }
        }
        public float Skill {
            get { return skill[ID]; }
            set { skill[ID] = value; }
        }
        public float TargetSize {
            get { return format[ID]; }
            set { format[ID] = value; }
        }
        public float Productivity {
            get { return productivity[ID]; }
            set { productivity[ID] = value; }
        }
        public float Motivation {
            get { return motivation[ID]; }
            set { motivation[ID] = value; }
        }
        public float Popularity {
            get { return popularity[ID]; }
            set { popularity[ID] = value; }
        }
        public float NextCreationSize {
            get { return nextCreationSize[ID]; }
            set { nextCreationSize[ID] = value; }
        }
        public float NextCreationProgress {
            get { return nextCreationProgress[ID]; }
            set { nextCreationProgress[ID] = value; }
        }

        void AddCreationProgress(float deltaTime) {
            NextCreationProgress += Productivity * Motivation * deltaTime/20;
            //Debug.Log(ID + ": Added some deltatime (" + deltaTime + ") " + NextCreationProgress + " / " + NextCreationSize);
            if (NextCreationProgress>=NextCreationSize ) {
                finishCreation();
                Skill += (GameManager.Random01() - 0.2f) * 0.2f;
                beginCreation();
                //NextCreationProgress = 0;
            }

        }
        public void GetNegativeFeedback(int CreationID) {
            //Decrease motivation.
            //Evaluate the chance to shug it off due to response being mostly positive.
            //If not ignored - drop Temper.
            //If Temper hit 0 - RageQuit.
        }
        public void GetPositiveFeedback(int CreationID) {
            //Get skill up.
            //Buff motivation.
        }
        void beginCreation()
        {
            NextCreationProgress = 0;
            float p = GameManager.Random01() * 0.8f - 0.4f;
            NextCreationSize = TargetSize * Mathf.Exp(Mathf.Tan(p * Mathf.PI) * 0.4f);
            //Debug.Log(ID+": Creation started "+ NextCreationSize);
        }
        void finishCreation()
        {
            long startTick = System.DateTime.Now.Ticks;

            Content newCreation = Content.Create();
            newCreation.Length = NextCreationSize;
            newCreation.Author = ID;
            newCreation.Dislikes = 0;
            newCreation.Likes = 0;
            newCreation.Topics = GameManager.RandomFlag() & Topic;
            
            newCreation.Views = 0;
            newCreation.Quality = Skill * (GameManager.Random01() + 0.5f) * TargetSize / NextCreationSize;
            newCreation.Density = (GameManager.Random01() * 2 + newCreation.Quality) / (Skill*10);

            //Debug.Log(ID + ": Creation finished - "+ newCreation.Length+";"+ newCreation.Density+" = "+ newCreation.Length * newCreation.Density);

            newCreation.Cluster = Cluster.AllocateToCluster(newCreation.Length * newCreation.Density);
            //Trickle Company hype up based on creator popularity?            
        }


        static bool RunCreatorProcess;
        static System.Threading.Thread creationProcessThread;
        public static void RunCreations()
        {
            RunCreatorProcess = true;
            if (creationProcessThread == null)
            {
                creationProcessThread = new System.Threading.Thread(IterateCreationProcess);
                creationProcessThread.IsBackground = true;
                creationProcessThread.Start();
            }
            else
            {
                // Thread should be running now...
            }
        }
        public static void StopCreations()
        {
            Debug.Log(";");
            RunCreatorProcess = false;
        }
        static void IterateCreationProcess()
        {
            long lastRefreshTick = System.DateTime.Now.Ticks;
            while (RunCreatorProcess)
            {

                long curTick = System.DateTime.Now.Ticks;
                float tDiff = (float)new System.TimeSpan(curTick - lastRefreshTick).TotalSeconds;
                lastRefreshTick = curTick;

                for (int i = 0; i < MaxCreators; i++)
                {
                    if (temper[i]!= -1)
                    {
                        GetContent(i).AddCreationProgress(tDiff);
                    }
                }
                
            }
            creationProcessThread = null;
            Debug.Log("Stopping Creation process;");
        }

        static bool runCreatorSpawn;
        static System.Threading.Thread spawnerThread;
        static float NewCreatorThreshold = 0;
        static float NewCreatorProgress;

        public static void RunSpawner() {
            runCreatorSpawn = true;
            if (spawnerThread == null ) {
                spawnerThread = new System.Threading.Thread(LoopCreationProcess);
                spawnerThread.IsBackground = true;
                spawnerThread.Start();

            } else {
                // Thread should be running now...
            }
        }
        public static void StopSpawner() {
            runCreatorSpawn = false;
        }

        static int creatorCount=0;

        public static void LoopCreationProcess() {
            long lastRefreshTick = System.DateTime.Now.Ticks;
            while (runCreatorSpawn) {
                long curTick = System.DateTime.Now.Ticks;
                float tDiff = (float)new System.TimeSpan( curTick - lastRefreshTick ).TotalSeconds;

                lastRefreshTick = curTick;
                NewCreatorProgress += tDiff;
                if (NewCreatorProgress> creatorCount) {

                    ContentCreator newCreator = Create();
                    newCreator.TargetSize = 1 + GameManager.Random01() * 3;
                    newCreator.Skill = GameManager.Random01() * 4;
                    newCreator.Productivity = GameManager.Random01() * 4;
                    newCreator.Popularity = GameManager.Random01()/2;
                    newCreator.Motivation = GameManager.Random01() * 3 + 3;
                    newCreator.Topic = GameManager.RandomFlag() & GameManager.RandomFlag();
                    newCreator.beginCreation();
                    newCreator.Temper = GameManager.RandomInt(3, 33);
                    NewCreatorProgress = 0;
                    creatorCount++;
                    Debug.Log("Creator " + newCreator.ID + " has registerred");
                }
                //Debug.Log(tDiff);

            }
            spawnerThread = null;
            Debug.Log("Stopping Creator Spawning;");
        }
    }
}