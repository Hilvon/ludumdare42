﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HostTycoon
{
    public class Server 
    {
        const int MaxServer = 1000;


        public static Server GetServer (int Id)
        {
            return new Server() { ID = Id };
        }
        static int GetVacantID()
        {
            for (int i = 0; i < MaxServer; i++)
            {
                if (clusterN[i] == -1) return i;
            }
            return 0;
        }
        public static void Reset()
        {
            for (int i = 0; i < MaxServer; i++)
            {
                clusterN[i] = -1;
            }
        }
        public static Server Create()
        {
            return GetServer(GetVacantID());
        }
        static int[] clusterN = new int[MaxServer];
        static int[] modelN = new int[MaxServer];
        static float[] wear = new float[MaxServer];


        int ID;
        public int ClusterN {
            get { return clusterN[ID]; }
            set { clusterN[ID] = value; }
        }
        public int ModelN {
            get { return modelN[ID]; }
            set { modelN[ID] = value; }
        }
        public float Wear {
            get { return wear[ID]; }
            set { wear[ID] = value; }
        }

        static void IterateServerWear()
        {
            for (int i = 0; i < MaxServer; i++)
            {
                if (clusterN[i] != -1)
                {

                }
            }
        }
    }
}