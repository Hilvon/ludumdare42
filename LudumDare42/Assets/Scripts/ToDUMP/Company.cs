﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HostTycoon
{
    public class Company : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {
            
        }

        struct test {
            public int A;
            public int B;
        }

        // Update is called once per frame
        void Update()
        {

        }
        public static bool IsRunning;
        System.Threading.Thread T;
        public void LaunchThread()
        {
            ContentCreator.Reset();
            Content.Reset();

            ContentCreator.RunSpawner();
            ContentCreator.RunCreations();

            
        }
        public void StopThread()
        {
            ContentCreator.StopSpawner();
            ContentCreator.StopCreations();
        }
        private void OnDestroy()
        {
            StopThread();
        }
    }
}