﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace HostTycoon
{
    /**
     * This is the class that handles launching the game process.
     * Though track if the game is runing. Has a call to start a game, and raises an event when game is ended
     * */
    public class GameManager : MonoBehaviour
    {
        static System.Random RNJesus = new System.Random();
        public static int RandomInt(int max = 100)
        {
            return RandomInt(0, max);
        }
        public static int RandomFlag()
        {
            return RNJesus.Next();
        }
        public static int RandomInt(int min, int max)
        {
            return RNJesus.Next(min, max);
        }
        public static float RandomFloat(float Max)
        {
            return Random01() * Max;
        }
        public static float Random01()
        {
            return RNJesus.Next(100000) / 100000f;
        }
        public void StartGame() {
            /*
             * Launch all supplementary threads.
             */
        }
        void StopGame()
        {
            /*
             * Stop all supplementary threads
             */
        }
    }
}