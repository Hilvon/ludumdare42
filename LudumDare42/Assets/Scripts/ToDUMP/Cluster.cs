﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HostTycoon
{
    public class Cluster
    {
        const int MaxClusters = 100;
        static int NextID;

        public static Cluster GetServer(int Id)
        {
            return new Cluster() { ID = Id };
        }
        static int GetVacantID()
        {
            return NextID++;
        }
        public static void Reset()
        {
            NextID = 0;
        }
        public static Cluster Create()
        {
            return GetServer(GetVacantID());
        }

        static float[] load;
        static float[] speedCapacity;
        static float[] capacity;
        static float[] storedData;
        int ID;

        static int filesStored;
        static float size;

        public static int AllocateToCluster(float size)
        {
            //Shold iterate through clusters to find one with most space available and return it's number
            //For now will just keep the number of cleations and their size
            filesStored++;
            Cluster.size += size;
            Debug.Log("files:" + filesStored + "; " + Cluster.size+"TB");
            return 1;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        static void UpdateCapacity ()
        {

        }
    }
}