﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This class represents a room tile. It can trace if it is being occupied by an item.
/// Can be free or Occupied.
/// Also can be reachable or blocked.
/// </summary>
public class RoomTile : MonoBehaviour {
    static HashSet<RoomTile> Reachable = new HashSet<RoomTile>();
    static RoomTile DoorTile;
    static List<RoomTile> AllTiles = new List<RoomTile>();
    public static void UpdateReachability()
    {
        Reachable.Clear();
        DoorTile.PropagateReacable();
        foreach(RoomTile T in AllTiles)
        {
            T.IsReachanble = Reachable.Contains(T);
            T.ErrRender.SetActive(!T.IsReachanble);
        }
    }
    public static Vector3 DoorPosition
    {
        get
        {
            if (DoorTile != null) return DoorTile.transform.position;
            else return Vector3.zero;
        }
    }
    public GameObject ErrRender;
    static HashSet<RoomTile> BlockedTiles = new HashSet<RoomTile>();
    static HashSet<RoomTile> CandidateTiles = new HashSet<RoomTile>();

    public static void ResetCandidated()
    {
        CandidateTiles.Clear();
    }
    public static void CommitCandidates()
    {
        BlockedTiles.UnionWith(CandidateTiles);
    }
    public static void ClearBlocked (params RoomTile [] Tiles) {
        BlockedTiles.ExceptWith(Tiles);
    }
    public static void ClearBlocked(IEnumerable<RoomTile> Tiles)
    {
        BlockedTiles.ExceptWith(Tiles);
    }

    public static bool AddCandidate(RoomTile Tile)
    {
        if (BlockedTiles.Contains(Tile)) return false;
        if (CandidateTiles.Contains(Tile)) return false;
        CandidateTiles.Add(Tile);
        return true;
    }

    public bool IsVacant(ItemTileBlock Blocker)
    {
        return blocker == null || blocker == Blocker;
    }
    public void unsetBlocker(ItemTileBlock Blocker)
    {
        if (blocker == Blocker) blocker = null;
    }
    public void setBlocker(ItemTileBlock Blocker)
    {
        blocker = Blocker;
    }
    ItemTileBlock blocker;
    public bool IsWindow;
    public bool IsDoor;

    public bool IsReachanble { get; private set; }

    public RoomTile Up;
    public RoomTile Down;
    public RoomTile Left;
    public RoomTile Right;

    void PropagateReacable()
    {
        Reachable.Add(this);
        if ((BlockedTiles.Contains(this) || CandidateTiles.Contains(this)) == false)
        {
            //Debug.Log("Tile " + name + " is reachable. Propagating further");
            if (Up!= null && Reachable.Contains(Up) == false) Up.PropagateReacable();
            if (Down != null && Reachable.Contains(Down) == false) Down.PropagateReacable();
            if (Left != null && Reachable.Contains(Left) == false) Left.PropagateReacable();
            if (Right != null && Reachable.Contains(Right) == false) Right.PropagateReacable();
        }
    }

    // Use this for initialization
    void Start () {
        if (IsDoor) DoorTile = this;
        AllTiles.Add(this);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
