﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Represents an item that needs to be placed in a room.
/// Holds several TileBlocks.
/// When PickedUp releases all the blocks.
/// When positioned - checks if all the blocks it is being dragget to are free. If yes - blocks them. If no - releases all blocked and moves to side pile.
/// 
/// Item is considered reachable if at least one tile ot is located in is reachable.
/// To procede to next stage you need to have all the currently placed items to be reacahble.
/// </summary>
public class Item : MonoBehaviour
{
    /*We need a way to track if level is complete;
     * For example we can have a level key stored in component
     *  
     * 
     */

    static List<Item> AllItems = new List<Item>();
    public static int UnreachedCount;
    static void CountUnreachable() {
        UnreachedCount = 0;
        foreach (Item I in AllItems) {
            if (I.gameObject.activeInHierarchy && !I.IsReachabe) UnreachedCount++;
            I.UpdateMaterial();
        }
    }

    //public static bool CheckAllSet()
    //{

    //}
    public Renderer Renderer;
    public bool IsDragged { get; set; }
    public bool IsTargeted { get; set; }

    public void UpdateMaterial() {
        System.Action<ModeDisplay> action = PickAction();
        foreach (ModeDisplay D in myDisplays)
        {
            action(D);
        }
    }

    System.Action<ModeDisplay> PickAction()
    {
        if (IsDragged)
        {
            if (FreeSpot)
            {
                return sendDragged;
            }
            return sendUnplacable;
        }
        else
        {
            if (IsTargeted)
            {
                return sendTarget;
            }
            if (!IsReachabe)
            {
                return sendUnreachable;
            }
            return sendNormal;
        }
    }

    static void sendNormal(ModeDisplay D) { D.ShowNormal(); }
    static void sendDragged(ModeDisplay D) { D.ShowDragged(); }
    static void sendUnplacable(ModeDisplay D) { D.ShowUnplacable(); }
    static void sendUnreachable(ModeDisplay D) { D.ShowUnreachable(); }
    static void sendTarget(ModeDisplay D) { D.ShowTargeted(); }

    //public Material DraggedMat, DraggedErrMat, NormalMat, UnreachableMat, TargetedMat;
    ModeDisplay[] _myDisplays;
    ModeDisplay[] myDisplays {
        get {
            if (_myDisplays == null) _myDisplays = GetComponentsInChildren<ModeDisplay>();
            return _myDisplays;
        }
    }

    bool IsReachabe
    {
        get
        {
            foreach (ItemTileBlock ITB in Blocks)
            {
                if (ITB.IsReachable) return true;
            }
            return false;
        }
    }
    public int Level;
    public string Label;
    public string Intro;
    public ItemTileBlock[] Blocks;
    // Use this for initialization
    void Start()
    {
        gameObject.SetActive(false);
        ItemSummonsList.RegisterItem(this);
        AllItems.Add(this);
        Blocks = GetComponentsInChildren<ItemTileBlock>(true);
    }

    // Update is called once per frame
    void Update()
    {
        
        //Debug.Log(res);
    }
    static bool IntCompareV3(Vector3 A, Vector3 B) {
        return (int)A.x == (int)B.x && (int)A.y == (int)B.y && (int)A.z == (int)B.z;
    }
    public void TryPosition(Vector3 newPos) {
        Vector3 IntPos = new Vector3((int)newPos.x, (int)newPos.y, (int)newPos.z);
        if (IntPos == transform.position ) {//} IntCompareV3(IntPos,transform.position)) {
            //Debug.Log("AreadyThere...");
        } else {
            transform.position = IntPos;
            Debug.Log("Position Change");
            EvaluateFit();
        }

    }
    public bool FreeSpot { get; private set; }
    public void EvaluateFit() {
        //Called when Item is moved to another tile, or rotated.
        //checks if the item is ove unoccupied tiles and not path is made unreachable.
        RoomTile.ResetCandidated();
        FreeSpot = true;
        foreach (ItemTileBlock TB in Blocks)
        {
            FreeSpot &= TB.UpdateTargetTile();
        }
        Debug.Log("New position evaluated by Blockers: " + FreeSpot);
        RoomTile.UpdateReachability();
        CountUnreachable();
        UpdateMaterial();
    }
    public void UnblockAll() {
        foreach (ItemTileBlock I in Blocks)
        {
            I.Unblock();
        }
    }

    public event System.Action<bool> OnEnabledChanged;

    private void OnEnable()
    {
        if (OnEnabledChanged != null) OnEnabledChanged(true);
    }
    private void OnDisable()
    {
        if (OnEnabledChanged != null) OnEnabledChanged(false);
    }
}
