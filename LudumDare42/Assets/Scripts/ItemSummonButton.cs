﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSummonButton : MonoBehaviour {

    Item TargetItem;
    public UnityEngine.UI.Text Label;
//    public int Level;

	// Use this for initialization
	public void Init (Item I) {
        TargetItem = I;
        Label.text = I.Label;
        TargetItem.OnEnabledChanged += RefreshVisibilityState;
        //TargetItem.gameObject.SetActive(false);
        RefreshVisibilityState(false);
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener( () =>
        {
            ItemMoveManager.MovedItem = TargetItem;
            TargetItem.IsDragged = true;
            TargetItem.transform.position = RoomTile.DoorPosition;
            TargetItem.gameObject.SetActive(true);
        });
	}

    public void LevelChanged() {
        RefreshVisibilityState(TargetItem.gameObject.activeInHierarchy);
    }
	
    void RefreshVisibilityState(bool EnableChanged) {
        if (LevelCounter.Current < TargetItem.Level) {
            gameObject.SetActive(false);
            return;
        }
        gameObject.SetActive(!EnableChanged);
    }
    private void OnDestroy()
    {
        if (TargetItem != null) TargetItem.OnEnabledChanged -= RefreshVisibilityState;
    }


    // Update is called once per frame
    void Update () {
		
	}
}
