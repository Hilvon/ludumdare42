﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ItemSummonsList : MonoBehaviour {
    public static List<ItemSummonButton> AllButtons = new List<ItemSummonButton>();
    public static void NewLevel() {
        foreach (ItemSummonButton B in AllButtons) {
            B.LevelChanged();
        }
    }
    public static bool CheckIfLevelComplete() {
        foreach(ItemSummonButton B in AllButtons) {
            if (B.gameObject.activeInHierarchy) return false;
        }
        return true;
    } 
    public ItemSummonButton Template;
    public static void RegisterItem(Item I) {
        if (main == null) {
            OnListStarted += (L) => { L.AddListItem(I); };
        }
        else {
            main.AddListItem(I);
        }
    }
    static ItemSummonsList main;
    static event System.Action<ItemSummonsList> OnListStarted;


    void AddListItem( Item I ) {
        ItemSummonButton newItem = Instantiate(Template,transform);
        newItem.Init(I);
        AllButtons.Add(newItem);
    }
	// Use this for initialization
	void Start () {
        main = this;
        if (OnListStarted != null) OnListStarted(this);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
