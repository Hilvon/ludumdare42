﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeDisplay : MonoBehaviour {
    public Material DraggedMat, DraggedErrMat, NormalMat, UnreachableMat, TargetedMat;

    Renderer _myR;
    Renderer myR { get {
            if (_myR == null) _myR = GetComponent<Renderer>();
            return _myR;
        } }
    public void ShowNormal()
    {
        myR.sharedMaterial = NormalMat;
    }
    public void ShowDragged()
    {
        myR.sharedMaterial = DraggedMat;
    }
    public void ShowUnplacable()
    {
        myR.sharedMaterial = DraggedErrMat;
    }
    public void ShowUnreachable()
    {
        myR.sharedMaterial = UnreachableMat;
    }
    public void ShowTargeted()
    {
        myR.sharedMaterial = TargetedMat;
    }
}
