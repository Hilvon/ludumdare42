﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Locates it's position in the room, and then can check if the tile it corresponds to is free or not.
/// </summary>
public class ItemTileBlock : MonoBehaviour {
    RoomTile TargetedTile;
    public bool IsReachable { get {
            if (TargetedTile == null) return false;
            return TargetedTile.IsReachanble;
        } }

    public void Unblock() {
        RoomTile.ClearBlocked(TargetedTile);
        TargetedTile = null;
    } 

    public bool UpdateTargetTile()
    {
        RaycastHit hitInfo;
        if (Physics.Raycast(transform.position, Vector3.down,out hitInfo, 999, LayerMask.GetMask("Floor")))
        {
            RoomTile hitTile = hitInfo.collider.GetComponentInParent<RoomTile>();
            if (hitTile!= null && !hitTile.IsWindow && RoomTile.AddCandidate( hitTile))
            {
                TargetedTile = hitTile;
                return true;
            }
        }
        return false;
    }

    private void OnDisable()
    {
        //if (TargetedTile != null)
        //{
        //    TargetedTile.unsetBlocker(this);
        //}
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
