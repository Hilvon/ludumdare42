﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMoveManager : MonoBehaviour {
    

    public static Item MovedItem;

    public KeyCode RotateKey;
	// Use this for initialization
	void Start () {
		
	}

    bool WasJump = false;
    bool CanceDown = false;
    bool Fire1Down = false;

    Dictionary<int, bool> Flags = new Dictionary<int, bool>() ;
    bool CheckFlag(int ID) {
        if (Flags.ContainsKey(ID) == false) return false;
        return Flags[ID];
    }
    void setFlag(int ID, bool val) {
        Flags[ID] = val;
    }

    void OnAxisDown(string AxisName, System.Action ToDo) {
        int FlagId = AxisName.GetHashCode();
        if (Input.GetAxis(AxisName) != 0)
        {
            if (!CheckFlag(FlagId))
            {
                if (ToDo != null) ToDo();
                setFlag (FlagId , true);
            }
        }
        else
        {
            setFlag(FlagId, false);
        }
    }

    Item LastTargeted;
	// Update is called once per frame
	void Update () {
		if (MovedItem != null)
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, 9999, LayerMask.GetMask("Floor"))) {
                MovedItem.TryPosition(hitInfo.point); //new Vector3((int)hitInfo.point.x,0, (int)hitInfo.point.z);
            }

            OnAxisDown("Jump", () => { 
                MovedItem.transform.Rotate(Vector3.up, 90);
                MovedItem.EvaluateFit();
            });

            OnAxisDown("Cancel", () =>
            {
                MovedItem.gameObject.SetActive(false);
                MovedItem.IsDragged = false;
                MovedItem = null;
            });

            OnAxisDown("Fire1", () => { 
                if (MovedItem.FreeSpot && Item.UnreachedCount == 0)
                {
                    Debug.Log("Item can be positioned");
                    RoomTile.CommitCandidates();
                    MovedItem.IsDragged = false;
                    MovedItem.UpdateMaterial();
                    MovedItem = null;
                    if (ItemSummonsList.CheckIfLevelComplete()) {
                        LevelCounter.IncreaseLevel();
                        ItemSummonsList.NewLevel();
                    }
                }
            });

        } else {
            RaycastHit hitInfo;
            Item TargetItem;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, 9999, LayerMask.GetMask("Items")))
            {
                TargetItem = hitInfo.collider.GetComponentInParent<Item>();
            } else {
                TargetItem = null;
            }

            if (TargetItem != LastTargeted)
            {
                if (LastTargeted != null)
                {
                    LastTargeted.IsTargeted = false;
                    LastTargeted.UpdateMaterial();
                }
                if (TargetItem != null)
                {
                    TargetItem.IsTargeted = true;
                    TargetItem.UpdateMaterial();
                }
                LastTargeted = TargetItem;
            }
            if (TargetItem != null)
            {
                OnAxisDown("Fire1", () =>
                {
                    TargetItem.IsDragged = true;
                    TargetItem.IsTargeted = false;
                    LastTargeted = null;
                    TargetItem.UpdateMaterial();
                    TargetItem.UnblockAll();
                    MovedItem = TargetItem;
                });
            }

        }
	}
}
